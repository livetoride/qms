<?php

$items[]=array();

class Item {
	public $country;
	public $timescale;
	public $vendor;
	public $units;
}

$file = fopen("data.csv","r");
$val = fgetcsv($file);
while(! feof($file))
{
	$val = fgetcsv($file);
	$item = new Item();
	$item->country = $val[0];
	$item->timescale = $val[1];
	$item->vendor = $val[2];
	$item->units = $val[3];
	
	if( is_numeric( $item->units ))
	if( isset( $items[ $item->vendor ] ))
	{
		$items[ $item->vendor ]->units += $item->units;
	} else {
		$items[ $item->vendor ] = $item;
	}
	
}
fclose($file);

foreach( $items as $i )
{
	echo $i->vendor . " \t\t ".$i->units."\n";
}
