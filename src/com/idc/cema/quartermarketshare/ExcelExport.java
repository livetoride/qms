package com.idc.cema.quartermarketshare;

/**
 * Class for excel export. Way I would use to implement this is in method comment.
 * @author Antonin Rykalsky
 */
public class ExcelExport {
	
	/**
	 * I would use Apache POI project. Specifically, according to this tutorial
	 * ( section Writing an excel file )
	 * http://howtodoinjava.com/2013/06/19/readingwriting-excel-files-in-java-poi-tutorial/
	 * 
	 * - linking POI library - org.apache.poi - using maven
	 * - creating sheet
	 * - go throuth passed Shares data
	 * - create a row in sheet and add cells in sheet
	 * - save somewhere / maybe second argument of method
	 * @param shares 
	 */
	public static void export( Shares shares )
	{
		/*
		//Blank workbook
        XSSFWorkbook workbook = new XSSFWorkbook();
         
        //Create a blank sheet
        XSSFSheet sheet = workbook.createSheet("Employee Data");
          
        //This data needs to be written (Object[])
        Map<String, Object[]> data = new TreeMap<String, Object[]>();
        data.put("1", new Object[] {"ID", "NAME", "LASTNAME"});
        data.put("2", new Object[] {1, "Amit", "Shukla"});
        data.put("3", new Object[] {2, "Lokesh", "Gupta"});
        data.put("4", new Object[] {3, "John", "Adwards"});
        data.put("5", new Object[] {4, "Brian", "Schultz"});
		*/
	}
}
