package com.idc.cema.quartermarketshare.entity;

public class Vendor {
	private String name;

	public Vendor(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}