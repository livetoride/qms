package com.idc.cema.quartermarketshare.entity;

public class Item implements Comparable<Item> {
	
	private Country country;
	private int year;
	private int quarter;
	private Vendor vendor;
	private double units;
	private double share;
	private double percent = -1;

	public Item( Vendor vendor, Country country, int year, int quarter, double units ) {
		this.vendor = vendor;
		this.country = country;
		this.year = year;
		this.quarter = quarter;
		this.units = units;
	}
	
	public void setPercentage(double total) {
		this.percent = units * 100 / total;
	}

	public void setShare(double share) {
		this.share = share;
	}

	public void raiseVendorUnits(Item i) {
		this.units += i.getUnits();
	}

	@Override
	public int compareTo(Item o) {
		return getVendor().getName().compareTo(o.getVendor().getName());
	}

	@Override
	public String toString() {
		return getVendor().getName() + " | " + getUnits();
	}
	
	//  <editor-fold defaultstate="collapsed" desc=" getters ">
	
	public Country getCountry() {
		return country;
	}
	
	public int getYear() {
		return year;
	}
	
	public double getPercent() {
		return percent;
	}
	
	public int getQuarter() {
		return quarter;
	}
	
	public String getQuarterString() {
		return "Q" + quarter;
	}
	
	public Vendor getVendor() {
		return vendor;
	}
	
	public double getUnits() {
		return units;
	}
	
	public double getShare() {
		return share;
	}
	
	
//</editor-fold>
	
}