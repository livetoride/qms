package com.idc.cema.quartermarketshare;

import com.idc.cema.quartermarketshare.entity.Item;
import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Main file for use quarter market share package. Just pass csv file path
 * and use Shares class functionality.
 * @author Antonin Rykalsky
 */
public class SharesFactory {
	
	
	public static Shares create( String inputfile ) throws FileNotFoundException, IOException
	{
		CSVReader reader = new CSVReader(new FileReader( inputfile ));
		String [] nextLine;
		
		Shares shares = new Shares();
		
		// ignore first line - csv header
		String[] columnMapping = reader.readNext();
		
		while ((nextLine = reader.readNext()) != null) {
			try {
				Item item = EntityFactory.create( nextLine, columnMapping );
				shares.addShare( item );
			} catch( ColumnOrderException e )
			{
				// There is wrong input into float value. It could be new lines in the end of file.
				// Some editors can put it into csv to so let me ignore it.
			}
		}
		
		shares.recountPercentage();
		
		return shares;
	}
	
}
