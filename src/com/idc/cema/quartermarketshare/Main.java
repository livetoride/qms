package com.idc.cema.quartermarketshare;

import com.idc.cema.quartermarketshare.entity.Item;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Example class of Quarter market shares csv factory usage
 * Use helper/macro to format units with two decimals
 * @author Antonin Rykalsky
 */
public class Main {
	
	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		// a. Load the input file and transform it into an object from which you can create a table.
		Shares shares = SharesFactory.create( "data.csv" );
		
		// lets findout total quantity shares
		System.out.println( "Total share is: " +  shares.getTotal() );
		
		// b. Ascertain the units and share values for a given vendor.
		System.out.println( "\nFS vendor" );
		System.out.println( shares.getShareByVendor("Fujitsu Siemens") );
		
		System.out.println("\nSorted by vendor");
		List<Item> byVendor = shares.getTableByVendor();
		SimpleRenderer.renderVendorFirst( byVendor );
		
		System.out.println("\nSorted by units");
		List<Item> byUnits = shares.getTableByUnits();
		SimpleRenderer.renderUnitFirst( byUnits );
		
		HtmlRenderer renderer = new HtmlRenderer();
		System.out.println( renderer.render( shares ) );
		
	}
}

