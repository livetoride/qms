package com.idc.cema.quartermarketshare;

import com.idc.cema.quartermarketshare.entity.Item;
import java.util.List;

/**
 * Simple HTML renderer.
 * @author Antonin Rykalsky
 */
public class HtmlRenderer {
	
	private final String table = "<table>";
	private final String tableEnd = "<table>";
	private final String row = "<tr>";
	private final String rowEnd = "</tr>";
	private final String col = "<td>";
	private final String colEnd = "</td>";
	private final String headcol = "<th>";
	private final String headcolEnd = "</th>";
	
	public String render( Shares shares )
	{
		List<Item> byVendor = shares.getTableByVendor();
		
		String html = "";
		
		html += table;
		
		html += row;
			html += headcol + "Vendor"+ headcolEnd;
			html += headcol + "Units"+ headcolEnd;
			html += headcol + "Share"+ headcolEnd;
		html += rowEnd;
		
		// print by vendor
		for (Item p : byVendor) {
			html += row;
			
			html += col;
			html += p.getVendor().getName();
			html += colEnd;
			
			html += col;
			html += p.getUnits();
			html += colEnd;
			
			html += col;
			html += p.getPercent();
			html += colEnd;
			
			html += rowEnd;
		}
		
		html += row;
			html += headcol + "Total"+ headcolEnd;
			html += headcol + shares.getTotal() + headcolEnd;
			html += headcol + "100%"+ headcolEnd;
		html += rowEnd;
		
		html += tableEnd;
		return html;
	}
	
}
