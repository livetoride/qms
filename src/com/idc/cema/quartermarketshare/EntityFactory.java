package com.idc.cema.quartermarketshare;

import com.idc.cema.quartermarketshare.entity.Item;
import com.idc.cema.quartermarketshare.entity.Country;
import com.idc.cema.quartermarketshare.entity.Vendor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class made for breaking csv row into the set of entities.
 * Classes Country and Vendor is very easy, holding one string param,
 * But just in case, of breaking it into more db tables.
 * @author AntoninRykalsky
 */
public class EntityFactory {

	public static Item create( String[] row, String[] columnMapping ) throws ColumnOrderException
	{
		try {
			Double.parseDouble( row[3] );
		} catch ( Exception e ) {
			throw new ColumnOrderException("There is wrong input into float value. It could be new lines in the end of file.");
		}
		
		Country country = new Country( row[0] );
		Vendor vendor = new Vendor( row[2] );
		
		Pattern pattern = Pattern.compile("([0-9]+) Q([0-9])");
		Matcher matcher = pattern.matcher(row[1]);
		
		int year = 0;
		int quarter = 0;
		while(matcher.find()){
			year = Integer.parseInt( matcher.group(1) );
			quarter = Integer.parseInt( matcher.group(2) );
		}
		double units=Double.parseDouble( row[3] );
	
		Item item = new Item( vendor, country, year, quarter, units );

		return item;
	}
	
}

