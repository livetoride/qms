
package com.idc.cema.quartermarketshare;

/**
 * Exception throught when empty row is readen or column 3 is not a float.
 * @author Tonik
 */
public class ColumnOrderException extends java.lang.Exception {

	public ColumnOrderException() {
		super();
	}

	public ColumnOrderException(String a) {
		super(a);
	}
}
