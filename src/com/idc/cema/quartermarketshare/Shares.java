package com.idc.cema.quartermarketshare;

import com.idc.cema.quartermarketshare.entity.Item;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Antonin Rykalsky
 */
public class Shares {

	private Map<String, Item> itemsMap;
	
	private double total=0.0;
	
	public Shares() {
		this.itemsMap = new HashMap<>();
	}

	public void addShare( Item item )
	{
		this.total += item.getUnits();
		
		Item tmpMapItem;
		try {
			tmpMapItem = this.itemsMap.get( item.getVendor().getName() );
			
			// vendor already exists
			tmpMapItem.raiseVendorUnits( item );
		} catch( NullPointerException e )
		{
			// put first item of this vendor
			this.itemsMap.put( item.getVendor().getName(), item );
		}
	}

	public double getTotal() {
		return total;
	}

	public List<Item> getTableByVendor()
	{
		// sortByVendor
		List<Item> itemByVendor = new ArrayList<>( this.itemsMap.values());
		Collections.sort(
			itemByVendor,
			(Item o1, Item o2) -> o1.getVendor().getName().compareTo(o2.getVendor().getName())
		);

		return itemByVendor;
	}
	
	public Item getShareByVendor( String vendor )
	{
		 return itemsMap.get( vendor );
	}
	
	public List<Item> getTableByUnits()
	{
		List<Item> itemByUnits = new ArrayList<>(itemsMap.values());
		
		Collections.sort(itemByUnits, (Item o1, Item o2) -> {
			Double d = o1.getUnits() - o2.getUnits();
			return d.intValue();
		});
		
		return itemByUnits;
	}

	public void recountPercentage()
	{
		List<Item> itemList = new ArrayList<>(itemsMap.values());
		itemList.stream().forEach((item) -> {
			item.setPercentage( total );
		});
	}
}
