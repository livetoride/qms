package com.idc.cema.quartermarketshare;

import com.idc.cema.quartermarketshare.entity.Item;
import java.util.List;

/**
 * Simple output renderer. Main thought is to keep CMD output out of the
 * model code.
 * @author Antonin Rykalsky
 */
public class SimpleRenderer {
	public static void renderUnitFirst( List<Item> items )
	{
		// print by vendor
		for (Item p : items) {
			System.out.format("%1$05.2f %2$20s \t %3$01.2f \n",
					p.getUnits(),
					p.getVendor().getName(),
					p.getPercent() );
		}
	}
	
	public static void renderVendorFirst( List<Item> items )
	{
		// print by vendor
		for (Item p : items) {
			System.out.format("%1$20s %2$05.2f \t %3$01.2f \n",
					p.getVendor().getName(),
					p.getUnits(),
					p.getPercent() );
		}
	}
}
